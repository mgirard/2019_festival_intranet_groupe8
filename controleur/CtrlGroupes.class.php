<?php


namespace controleur;


use modele\dao\Bdd;
use modele\dao\GroupeDAO;
use vue\groupes\VueListeGroupes;
use vue\groupes\VueSaisieGroupes;
use modele\metier\Groupe;
use vue\groupes\VueSuprimerGroupes;

class CtrlGroupes extends ControleurGenerique
{

    /**
     * Action par défaut. Devra être implémentée par chaque contrôleur
     */
    public function defaut()
    {
        $this->liste();
    }

    /**
     * Afficher tous les groupes
     */
    public function liste() {
        $vue = new VueListeGroupes();
        $this->vue = $vue;
        Bdd::connecter();
        // Récuperation et instanciation des groupes
        $vue->setGroupes(GroupeDAO::getAll());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Groupes");
        $this->vue->afficher();
    }
    /**  controleur=groupes&action=modifier
     * Ajout du formulaire de modification d'un groupe
     */
    public function modifier(){
        $idGroup = $_GET["id"];
        $laVue = new VueSaisieGroupes();
        $this->vue = $laVue;
        //connection à la base de données
        Bdd :: connecter();
        /* @var Groupe $leGroupe */
        $leGroupe = GroupeDAO::getOneById($idGroup);
        $this->vue->setGroupe($leGroupe);
        $laVue->setAction("modifier");
        $laVue->setActionEnvoyer("validerModifier");
        $laVue->setMessage("Modifier le groupe : " . $leGroupe->getNom() . " (" . $leGroupe->getId() . ")");
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();

    }

    /** controleur = groupe & action = validerModifier
     * modifier un groupe dans la base de données après la saisie
     */
    public function validerModifier(){
        //connection à la base de données
        Bdd :: connecter();
        $identiteResponsable = $_REQUEST['identiteResponsable'];
        $adressePostale = $_REQUEST['adressePostale'];
        $hebergement = strtoupper($_REQUEST['hebergement']);
        $nombrePersonne = intval($_REQUEST['nombrePersonnes']);
        if(empty($identiteResponsable)) $identiteResponsable = null;
        if(empty($adressePostale)) $adressePostale = null;
        if(strlen($hebergement)!=1 || !($hebergement == 'N' || $hebergement == 'O' )) {
            GestionErreurs::ajouter("L'hébergement n'est pas bon !");
        }
        if ($nombrePersonne < 0) {
            GestionErreurs::ajouter("Le nombre n'est pas assez grand !");
        }
        $unGroup = new Groupe($_REQUEST['id'], $_REQUEST['nom'],$identiteResponsable,$adressePostale,$nombrePersonne, $_REQUEST['nomPays'],$hebergement);
        if (GestionErreurs::nbErreurs()>0){
            $laVue = new VueSaisieGroupes();
            $this->vue = $laVue;
            $laVue->setAction("modifier");
            $laVue->setActionEnvoyer("validerModifier");
            $laVue->setMessage("Modifier Groupe");
            $laVue->setGroupe($unGroup);
            parent::controlerVueAutorisee();
            $this->vue->setTitre("Festival - Groupe");
            $this->vue->afficher();
        }else{
            /* @var Groupe $unGroup  : récupération du contenu du formulaire et instanciation d'un groupe */
            GroupeDao::update($unGroup->getId(),$unGroup);
            header("Location: index.php?controleur=groupes");
        }
       
        

    }

    /** controleur= groupe & action=creer
     * Afficher le formulaire d'ajout d'un groupe     */
    public function creer() {
        $laVue = new VueSaisieGroupes();
        $this->vue = $laVue;
        $laVue->setAction("creer");
        $laVue->setActionEnvoyer("validerCreer");
        $laVue->setMessage("Nouveau Groupe");
        // En création, on affiche un formulaire vide
        /* @var Groupe $unGroup */
        $unGroup = new Groupe("", "", "", "", "", "", "");
        $laVue->setGroupe($unGroup);
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Groupe");
        $this->vue->afficher();
    }

    /** controleur= groupe & action=validerCreer
     * ajout d'un groupe dans la base de données d'après la saisie    */
    public function validerCreer() {
        Bdd::connecter();
        $identiteResponsable = $_REQUEST['identiteResponsable'];
        $adressePostale = $_REQUEST['adressePostale'];
        $hebergement = strtoupper($_REQUEST['hebergement']);
        $nombrePersonne = intval($_REQUEST['nombrePersonnes']);
        if(empty($identiteResponsable)) $identiteResponsable = null;
        if(empty($adressePostale)) $adressePostale = null;
        if(strlen($hebergement)!=1 || !($hebergement == 'N' || $hebergement == 'O' )) {
            GestionErreurs::ajouter("L'hébergement n'est pas bon !");
        }
        if ($nombrePersonne < 0) {
            GestionErreurs::ajouter("Le nombre n'est pas assez grand !");
        }
        $unGroup = new Groupe($_REQUEST['id'], $_REQUEST['nom'],$identiteResponsable,$adressePostale,$nombrePersonne, $_REQUEST['nomPays'],$hebergement);
        if (GestionErreurs::nbErreurs()>0){
            $laVue = new VueSaisieGroupes();
            $this->vue = $laVue;
            $laVue->setAction("creer");
            $laVue->setActionEnvoyer("validerCreer");
            $laVue->setMessage("Nouveau Groupe");
            $laVue->setGroupe($unGroup);
            parent::controlerVueAutorisee();
            $this->vue->setTitre("Festival - Groupe");
            $this->vue->afficher();
        }else{
            /* @var Groupe $unGroup  : récupération du contenu du formulaire et instanciation d'un groupe */
            GroupeDao::insert($unGroup);
            header("Location: index.php?controleur=groupes");
        }
    }
      public function supprimer() {
        $id = $_GET["id"];
        $this->vue = new VueSuprimerGroupes();
        // Lire dans la BDD les données du groupe à supprimer
        Bdd::connecter();
        $this->vue->setUnGroupe(GroupeDAO::getOneById($id));
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - groupes");
        $this->vue->afficher();
    }
    public function validerSupprimer() {
        Bdd::connecter();
        if (!isset($_GET["id"])) {
            // pas d'identifiant fourni
            GestionErreurs::ajouter("Il manque l'identifiant du groupe à supprimer");
        } else {
            // suppression du groupe d'après son identifiant
            GroupeDAO::delete($_GET["id"]);
        }
        // retour à la liste des groupes
        header("Location: index.php?controleur=groupes&action=liste");
    }
}