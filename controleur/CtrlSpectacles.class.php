<?php

/**
 * Contrôleur de gestion des offres d'hébergement
 * @author prof
 * @version 2018
 */

namespace controleur;

use modele\dao\Bdd;
use modele\dao\SpectacleDAO;
use vue\spectacles\VueListeSpectacle;

class CtrlSpectacles extends ControleurGenerique
{

    public function defaut()
    {
        $this->consulter();
    }

    function consulter()
    {
        $this->vue = new VueListeSpectacle();
        Bdd::connecter();
        $this->vue->setSpectaclesSortedByDate(SpectacleDAO::getAllSortByDate());
        parent::controlerVueAutorisee();
        $this->vue->setTitre("Festival - Spectacles");
        $this->vue->afficher();
    }

}
