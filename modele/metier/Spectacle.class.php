<?php

namespace modele\metier;

class Spectacle
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var Groupe
     */
    private $idGroup;

    /**
     * @var Lieu
     */
    private $idLieu;

    /**
     * @var string
     */
    private $date;

    /**
     * @var string
     */
    private $heureDeb;

    /**
     * @var string
     */
    private $heureFin;

    /**
     * Spectacle constructor.
     * @param int $id
     * @param Groupe $idGroup
     * @param Lieu $idLieu
     * @param string $date
     * @param string $heureDeb
     * @param string $heureFin
     */
    public function __construct(int $id, Groupe $idGroup, Lieu $idLieu, string $date, string $heureDeb, string $heureFin)
    {
        $this->id = $id;
        $this->idGroup = $idGroup;
        $this->idLieu = $idLieu;
        $this->date = $date;
        $this->heureDeb = $heureDeb;
        $this->heureFin = $heureFin;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }


    /**
     * @return Groupe
     */
    public function getGroupe(): Groupe
    {
        return $this->idGroup;
    }

    /**
     * @param Groupe $idGroup
     */
    public function setGroupe(Groupe $idGroup): void
    {
        $this->idGroup = $idGroup;
    }

    /**
     * @return Lieu
     */
    public function getLieu(): Lieu
    {
        return $this->idLieu;
    }

    /**
     * @param Lieu $idLieu
     */
    public function setLieu(Lieu $idLieu): void
    {
        $this->idLieu = $idLieu;
    }

    /**
     * @return string
     */
    public function getDate(): string
    {
        return $this->date;
    }

    /**
     * @param string $date
     */
    public function setDate(string $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getHeureDeb(): string
    {
        return $this->heureDeb;
    }

    /**
     * @param string $heureDeb
     */
    public function setHeureDeb(string $heureDeb): void
    {
        $this->heureDeb = $heureDeb;
    }

    /**
     * @return string
     */
    public function getHeureFin(): string
    {
        return $this->heureFin;
    }

    /**
     * @param string $heureFin
     */
    public function setHeureFin(string $heureFin): void
    {
        $this->heureFin = $heureFin;
    }


}

   

    
