<?php


namespace modele\dao;


use modele\metier\Lieu;
use PDO;
use PDOStatement;

class LieuDAO
{

    /**
     * Instancier un objet de la classe Lieu à partir d'un enregistrement de la table Lieu
     * @param array $enr
     * @return Lieu
     */
    protected static function enregVersMetier(array $enr)
    {
        return new Lieu($enr["ID"], $enr["NOM"], $enr["ADRESSE"], $enr["CAPACITE"]);
    }

    /**
     * Valorise les paramètres d'une requête préparée avec l'état d'un objet Lieu
     * @param Lieu $lieu
     * @param PDOStatement $stmt
     */
    protected static function metierVersEnreg(Lieu $lieu, PDOStatement $stmt)
    {
        // On utilise bindValue plutôt que bindParam pour éviter des variables intermédiaires
        // Note : bindParam requiert une référence de variable en paramètre n°2 ;
        // avec bindParam, la valeur affectée à la requête évoluerait avec celle de la variable sans
        // qu'il soit besoin de refaire un appel explicite à bindParam
        $stmt->bindValue(':id', $lieu->getId());
        $stmt->bindValue(':nom', $lieu->getNom());
        $stmt->bindValue(':adresse', $lieu->getAdresse());
        $stmt->bindValue(':capacite', $lieu->getCapacite());
    }

    /**
     * Retourne la liste de tous les lieux
     * @return array tableau d'objets de type Lieu
     */
    public static function getAll()
    {
        $lesObjets = array();
        $requete = "SELECT * FROM Lieu";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // Tant qu'il y a des enregistrements dans la table
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                //ajoute un nouveau groupe au tableau
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }

    /**
     * Recherche un lieu selon la valeur de son identifiant
     * @param string $id
     * @return Lieu le lieu trouvé ; null sinon
     */
    public static function getOneById($id)
    {
        $objetConstruit = null;
        $requete = "SELECT * FROM Lieu WHERE id = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(":id", $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }

    /**
     * Insérer un nouvel enregistrement dans la table à partir de l'état d'un objet métier
     * @param Lieu $lieu
     * @return bool
     */
    public static function insert(Lieu $lieu)
    {
        $requete = "INSERT INTO Lieu VALUES (:id, :nom, :adresse, :capacite)";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($lieu, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }
    /**
     * modifier un lieu (nom, adresse, capacite) en fonction de son id
     * @param Lieu $lieu
     * @return bool
     */
    public static function update($id, Lieu $lieu)
    {
        $ok = false;
        $requete = "UPDATE Lieu SET nom = :nom, adresse = :adresse, capacite = :capacite WHERE id = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($lieu, $stmt);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }
/**
     * supprimez un lieu en fonction de son id
     * @param Lieu $lieu
     * @return bool
     */
    public static function delete($id)
    {
        $ok = false;
        $requete = "DELETE FROM Lieu WHERE id = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(":id", $id);
        $ok = $stmt->execute();
        $ok = $ok && ($stmt->rowCount() > 0);
        return $ok;
    }
}