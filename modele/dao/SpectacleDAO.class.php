<?php

namespace modele\dao;

use modele\metier\Spectacle;
use PDO;
use PDOStatement;

/**
 * Description of SpectacleDAO
 *
 * @author Antoine
 */
class SpectacleDAO
{
    protected static function enregVersMetier(array $enreg)
    {
        $id = $enreg["ID"];
        $idGroup = $enreg['IDGROUPE'];
        $idLieu = $enreg['IDLIEU'];
        $date = $enreg['DATE'];
        $heureDeb = $enreg['HEUREDEBUT'];
        $heureFin = $enreg['HEUREFIN'];

        $unSpect = new Spectacle($id, GroupeDAO::getOneById($idGroup), LieuDAO::getOneById($idLieu), $date, $heureDeb, $heureFin);

        return $unSpect;
    }

    protected static function metierVersEnreg(Spectacle $objetMetier, PDOStatement $stmt)
    {
        // On utilise bindValue plutôt que bindParam pour éviter des variables intermédiaires
        // Note : bindParam requiert une référence de variable en paramètre n°2 ; 
        // avec bindParam, la valeur affectée à la requête évoluerait avec celle de la variable sans
        // qu'il soit besoin de refaire un appel explicite à bindParam
        $stmt->bindValue(':idGroup', $objetMetier->getIdGroup());
        $stmt->bindValue(':idLieu', $objetMetier->getidLieu());
        $stmt->bindValue(':date', $objetMetier->getdate());
        $stmt->bindValue(':heureDeb', $objetMetier->getheureDeb());
        $stmt->bindValue(':heureFin', $objetMetier->getheureFin());
    }

    public static function getAll()
    {
        $lesObjets = array();
        $requete = "SELECT * FROM Spectacle";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            // Pour chaque enregisterement
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                // instancier un Spectacle et l'ajouter au tableau
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }

    /**
     * Retourne les spectacles triés par leur date
     * @return Spectacle[]
     */
    public static function getAllSortByDate()
    {
        $spectacles = [];
        $query = "SELECT * FROM Spectacle ORDER BY date";
        $statement = Bdd::getPdo()->prepare($query);
        if ($statement->execute()) {
            $lastDate = "";
            while ($data = $statement->fetch(PDO::FETCH_ASSOC)) {
                $spectacle = self::enregVersMetier($data);
                if ($spectacle->getDate() !== $lastDate) $lastDate = $spectacle->getDate();
                $spectacles[$lastDate][] = $spectacle;
            }
        }
        return $spectacles;
    }

    public static function getOneById(int $id)
    {
        $objetConstruit = null;
        $requete = "SELECT * FROM Spectacle WHERE id = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }

    /**
     * Insérer un nouvel enregistrement dans la table à partir de l'état d'un objet métier
     * @param Spectacle
     * $objet objet métier à insérer
     * @return boolean =FALSE si l'opération échoue
     */
    public static function insert(Spectacle $objet)
    {
        $requete = "INSERT INTO Spectacle (idGroupe, idLieu, date, heureDebut, heureFin) VALUES (:idGroup, :idLieu, :date, :heureDeb, :heureFin)";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    /**
     * Mettre à jour enregistrement dans la table à partir de l'état d'un objet métier
     * @param int identifiant de l'enregistrement à mettre à jour
     * @param Spectacle $objet objet métier à mettre à jour
     * @return boolean =FALSE si l'opérationn échoue
     */
    public static function update(int $id, Spectacle $objet)
    {
        $ok = false;
        $requete = "UPDATE  Spectacle SET idGroupe=:idGroup, idLieu=:idLieu, date=:date, heureDebut=:heureDeb, heureFin=:heureFin
         WHERE id=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    /**
     * supprimez un enregistrement dans la table à partir de l'état d'un objet métier
     * @param $id métier à supprimer
     * @return boolean =FALSE si l'opérationn échoue
     */
    public static function delete($id)
    {
        $ok = false;
        $requete = "DELETE FROM Spectacle WHERE id=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(":id", $id);
        $ok = $stmt->execute();
        $ok = $ok && ($stmt->rowCount() > 0);
        return $ok;
    }
}