<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>SpectacleDAOTest : test</title>
</head>

<body>

<?php

use controleur\Session;
use modele\dao\Bdd;
use modele\dao\SpectacleDAO;
use modele\metier\Spectacle;

require_once __DIR__ . '/../../includes/autoload.inc.php';

$id = 2;
Session::demarrer();
Bdd::connecter();

echo "<h2>1- SpectacleDAO</h2>";

// Test n°1
echo "<h3>Test getOneById</h3>";
try {
    $objet = SpectacleDAO::getOneById($id);
//    var_dump($objet);
} catch (Exception $ex) {
    echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
}

// Test n°2
echo "<h3>2- getAll</h3>";
try {
    $lesObjets = SpectacleDAO::getAll();
    var_dump($lesObjets);
} catch (Exception $ex) {
    echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
}

echo "<h3>2.1 - getAllSortByDate</h3>";
try {
    $objects = SpectacleDAO::getAllSortByDate();
    var_dump($objects);
} catch (Exception $exception) {
    echo "<h4>*** échec de la requête ***</h4>" . $ex->getMessage();
}

// Test n°3
echo "<h3>3- insert</h3>";
try {
    $id = 6;
    $objet = new Spectacle($id, "g047", 2, "2019-03-17", "10:00", "15:00");
    $ok = SpectacleDAO::insert($objet);
    if ($ok) {
        echo "<h4>ooo réussite de l'insertion ooo</h4>";
        $objetLu = SpectacleDAO::getOneById($id);
        var_dump($objetLu);
    } else {
        echo "<h4>*** échec de l'insertion ***</h4>";
    }
} catch (Exception $e) {
    echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
}

// Test n°3-bis
echo "<h3>3- insert déjà présent</h3>";
try {
    $objet = new Spectacle($id, "g047", 2, "2019-03-17", "10:00", "15:00");
    $ok = SpectacleDAO::insert($objet);
    if ($ok) {
        echo "<h4>*** échec du test : l'insertion ne devrait pas réussir  ***</h4>";
        $objetLu = SpectacleDAO::getOneById($id);
        var_dump($objetLu);
    } else {
        echo "<h4>ooo réussite du test : l'insertion a logiquement échoué ooo</h4>";
    }
} catch (Exception $e) {
    echo "<h4>ooo réussite du test : la requête d'insertion a logiquement échoué ooo</h4>" . $e->getMessage();
}

// Test n°4
echo "<h3>4- update</h3>";
try {
    $objet->setDate("2019-05-16");
    $objet->setHeureDeb("09:00");
    $objet->setHeureFin("10:00");
    $ok = SpectacleDAO::update($id, $objet);
    if ($ok) {
        echo "<h4>ooo réussite de la mise à jour ooo</h4>";
        $objetLu = SpectacleDAO::getOneById($id);
        var_dump($objetLu);
    } else {
        echo "<h4>*** échec de la mise à jour ***</h4>";
    }
} catch (Exception $e) {
    echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
}

// Test n°5
echo "<h3>5- delete</h3>";
try {
    $ok = SpectacleDAO::delete($id);
    if ($ok) {
        echo "<h4>ooo réussite de la suppression ooo</h4>";
    } else {
        echo "<h4>*** échec de la suppression ***</h4>";
    }
} catch (Exception $e) {
    echo "<h4>*** échec de la requête ***</h4>" . $e->getMessage();
}


Bdd::deconnecter();
Session::arreter();
?>


</body>
</html>
