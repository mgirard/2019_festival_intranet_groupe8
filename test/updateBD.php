<?php

$pdo = new PDO("mysql:host=localhost;dbname=festival", "festival", "secret");
//$pdo->query("ALTER TABLE utilisateur ALTER COLUMN mdp varchar(255)");
foreach ($pdo->query("SELECT id, mdp FROM Utilisateur")->fetchAll() as $u) {
    $s = $pdo->prepare("UPDATE Utilisateur SET mdp = :m WHERE id = :i");
    $s->bindValue(":m", password_hash($u["mdp"], PASSWORD_DEFAULT), PDO::PARAM_STR);
    $s->bindValue(":i", $u["id"], PDO::PARAM_INT);
    $s->execute();
}