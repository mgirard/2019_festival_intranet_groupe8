<?php
/**
 * Description Page de consultation de la liste des spectacles
 */

namespace vue\spectacles;

use vue\VueGenerique;

class VueListeSpectacle extends VueGenerique
{

    /**
     * @var array
     */
    private $spectacles;

    public function __construct()
    {
        parent::__construct();
    }

    public function afficher()
    {
        include $this->getEntete();
        ?>
        <br/>
        <h1>Programme par jours</h1>
        <?php
        foreach ($this->spectacles as $key => $spectacleArray) {
            ?>
            <h3><?= $key ?></h3>
            <table width="70%" cellspacing="0" cellpadding="0" class="tabQuadrille">
                <tr class="enTeteTabQuad">
                    <th>Lieu</th>
                    <th>Groupe</th>
                    <th>Heure début</th>
                    <th>Heure fin</th>
                    <th></th>
                    <th></th>
                </tr>
                <?php
                foreach ($spectacleArray as $spectacle) {
                    ?>
                    <tr class="ligneTabNonQuad">
                        <td width="25%"><?= $spectacle->getLieu()->getAdresse() ?></td>
                        <td width="30%"><?= $spectacle->getGroupe()->getNom() ?></td>
                        <td style="text-align: center;"><?= $spectacle->getHeureDeb() ?></td>
                        <td style="text-align: center;"><?= $spectacle->getHeureFin() ?></td>
                        <td width="5%" style="text-align: center;"><a href="#">Modifier</a></td>
                        <td width="5%" style="text-align: center;"><a href="#">Supprimer</a></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
            <?php
        }
        ?>
        <?php
        include $this->getPied();
    }

    function setSpectaclesSortedByDate(array $spectacles)
    {
        $this->spectacles = $spectacles;
    }

}
