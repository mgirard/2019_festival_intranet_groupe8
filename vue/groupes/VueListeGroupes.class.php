<?php

namespace vue\groupes;


use vue\VueGenerique;

class VueListeGroupes extends VueGenerique
{

    /**
     * @var array Liste des groupes
     */
    private $groupes;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     *  Afficher la vue signifie l'inclure au flux de sortie HTML
     */
    public function afficher()
    {
        include $this->getEntete();
        ?>
            <br>
            <table width="55%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">

            <tr class="enTeteTabNonQuad">
                <td colspan="4"><strong>Groupes</strong></td>
            </tr>
            
            <!--Pour chaque groupe -->
            
        <?php
       foreach ($this->groupes as $groupe) {
            $id = $groupe->getId();
            $nom = $groupe->getNom();
            ?>
            <tr class="ligneTabNonQuad">
                <td width="52%"><?= $nom ?></td>

              

                <td width="16%" align="center">
                    <a href="index.php?controleur=groupes&action=modifier&id=<?= $id ?>">
                        Modifier
                    </a>
                    
                    
                </td>
                <td width="16%" align="center">
                    <a href="index.php?controleur=groupes&action=supprimer&id=<?= $groupe->getId()?>">
                        Supprimer
                    </a>
                </td>

                <?php
                
                ?>
            </tr>
            <?php
        }
        ?>
    </table> 
    <br>
    <a href='index.php?controleur=groupes&action=creer' style="background-color:#FFFFFF; font-size:18px;">
            Création d'un groupe
        </a>      
        <?php
        include $this->getPied();
        }
    

    /**
     * @return array
     */
    public function getGroupes(): array
    {
        return $this->groupes;
    }

    /**
     * @param array $groupes
     */
    public function setGroupes(array $groupes)
    {
        $this->groupes = $groupes;
    }
    
}
