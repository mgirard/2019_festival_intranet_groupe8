<?php

namespace vue\groupes;

use modele\metier\Groupe;
use vue\VueGenerique;

class VueSaisieGroupes extends VueGenerique
{

    /**
     * @var string
     */
    private $action;

    /**
     * @var string
     */
    private $actionEnvoyer;

    /**
     * @var Groupe
     */
    private $groupe;

    /**
     * @var string
     */
    private $message;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     *  Afficher la vue signifie l'inclure au flux de sortie HTML
     */
    public function afficher()
    {
        include $this->getEntete();
        ?>
        <form method="post" action="index.php?controleur=groupes&action=<?= $this->getActionEnvoyer() ?>">
            <br>
            <table width="40%" cellspacing="0" cellpadding="0" class="tabNonQuadrille">
                <tr class="enTeteTabNonQuad">
                    <td colspan="3"><strong><?= $this->getMessage() ?></strong></td>
                </tr>
                <?php
                if ($this->action === "creer") {
                    ?>
                    <tr class="ligneTabNonQuad">
                        <td>Id : </td>
                        <td><input type="text" value="<?= $this->getGroupe()->getId() ?>" name="id"></td>
                    </tr>
                    <?php
                } else {
                    ?>
                    <tr>
                        <td><input type="hidden" value="<?= $this->getGroupe()->getId() ?>" name="id"></td>
                    </tr>
                    <?php
                }
                ?>
                <tr class="ligneTabNonQuad">
                    <td>Nom : </td>
                    <td><input type="text" value="<?= $this->getGroupe()->getNom() ?>" name="nom"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td>Identite Responsable : </td>
                    <td><input type="text" value="<?= $this->getGroupe()->getIdentite() ?>" name="identiteResponsable"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td>Adresse Postale : </td>
                    <td><input type="text" value="<?= $this->getGroupe()->getAdresse() ?>" name="adressePostale"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td>Nombre Personnes : </td>
                    <td><input type="number" value="<?= $this->getGroupe()->getNbPers() ?>" name="nombrePersonnes"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td>Nom Pays : </td>
                    <td><input type="text" value="<?= $this->getGroupe()->getNomPays() ?>" name="nomPays"></td>
                </tr>
                <tr class="ligneTabNonQuad">
                    <td>Hebergement : </td>
                    <td><input type="text" value="<?= $this->getGroupe()->getHebergement() ?>" name="hebergement"></td>
                </tr>
            </table>
            <table align="center" cellspacing="15" cellpadding="0">
                <tr>
                    <td align="right"><input type="submit" value="Valider" name="valider">
                    </td>
                    <td align="left"><input type="reset" value="Annuler" name="annuler"></td>
                </tr>
            </table>
            <a href="index.php?controleur=groupes&action=liste">Retour</a>
        </form>
        <?php
        include $this->getPied();
    }

    /**
     * @return string
     */
    public function getAction(): string
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction(string $action)
    {
        $this->action = $action;
    }

    /**
     * @return string
     */
    public function getActionEnvoyer(): string
    {
        return $this->actionEnvoyer;
    }

    /**
     * @param string $actionEnvoyer
     */
    public function setActionEnvoyer(string $actionEnvoyer)
    {
        $this->actionEnvoyer = $actionEnvoyer;
    }

    /**
     * @return Groupe
     */
    public function getGroupe(): Groupe
    {
        return $this->groupe;
    }

    /**
     * @param Groupe $groupe
     */
    public function setGroupe(Groupe $groupe)
    {
        $this->groupe = $groupe;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message)
    {
        $this->message = $message;
    }
}