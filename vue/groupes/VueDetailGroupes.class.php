<?php

namespace vue\groupes;

use vue\VueGenerique;

/**
 * Class VueDetailGroupes
 * @package vue\groupes
 */
class VueDetailGroupes extends VueGenerique
{
    /** @var Groupe identificateur de l'établissement à afficher */
    private $unGroupe;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     *  Afficher la vue signifie l'inclure au flux de sortie HTML
     */
    public function afficher()
    {
        include $this->getEntete();
        ?>
        <br>
        <table width='60%' cellspacing='0' cellpadding='0' class='tabNonQuadrille'>
            <tr class='enTeteTabNonQuad'>
                <td colspan='3'><strong><?= $this->unGroupe->getNom() ?></strong></td>
            </tr>
            <tr class='ligneTabNonQuad'>
                <td width='20%'> Id:</td>
                <td><?= $this->unGroupe->getId() ?></td>
            </tr>
        </table>
    <?php }
}