<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace vue\groupes;
use vue\VueGenerique;
use modele\metier\Groupe;

/**
 * Description of VueSuprimerGroupes
 *
 * @author Antoi
 */
class VueSuprimerGroupes extends VueGenerique {
    //put your code here
     /** @var Groupe type de chambre à modifier */
    private $unGroupe;
    public function __construct() {
        parent::__construct();
    }

    public function afficher() {
        include $this->getEntete();
        ?>
        <br><center>Voulez-vous vraiment supprimer le groupe
        <?= $this->unGroupe->getId() . " " . $this->unGroupe->getNom() ?> ?
            <h3><br>
                <a href="index.php?controleur=Groupes&action=validerSupprimer&id=<?= $this->unGroupe->getId() ?>">
                    Oui</a>
                &nbsp; &nbsp; &nbsp; &nbsp;
                <a href="index.php?controleur=Groupes">Non</a></h3></center>
        <?php
        include $this->getPied();
    }
    function getUnGroupe(): Groupe {
        return $this->unGroupe;
    }
    function setUnGroupe(Groupe $unGroupe) {
        $this->unGroupe = $unGroupe;
    }



}
